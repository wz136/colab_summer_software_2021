import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

# from flaskr.db import get_db
from .models import Address

bp = Blueprint('address', __name__, url_prefix='/address',template_folder='templates')
@bp.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@bp.route("/template")
def show_template():
  return render_template("address/example.html", some_value="here is a value", first_address=Address.query.first())